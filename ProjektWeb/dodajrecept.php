<?php
include("connect.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dodavanje recepta</title>
    <!--font-->
    <link href='https://fonts.googleapis.com/css?family=Ewert' rel='stylesheet'>
    <!--bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <div class="overlay">
    <header class="heade">
        <div>
            <h2>Dodaj recept</h2>
        </div>
    </header>

    <?php if(isset($_SESSION["loggedin"])===true){ ?>
    <div class="kontenjer">
        <form action="upload.php" method="post" enctype="multipart/form-data">
         <?php $userid=$_SESSION["id"]; ?>
            <div>
                <input type="text" name="naziv" placeholder="Ovdje unesite naziv recepta" required="required">
            </div>
            <br>
            <div>
                <textarea name="sastojci" rows="2" cols="30" placeholder="Ovdje unesi sastojke potrebne za recept" required="required"></textarea>
            </div>
            <br>
            <div>
                <textarea name="opis" rows="10" cols="60" placeholder="Ovdje unesi svoj recept :)" required="required"></textarea>
            </div>
            <div>        
                Unesi sliku:<br>
                <input type="file" name="fileToUpload" id="fileToUpload" required="required"><br>
                <input type="submit" value="Predaj recept" name="submit" id="predaj">
            </div>
        </form>
        
    </div>   
    <?php } ?>
    <?php if(isset($_SESSION["loggedin"])===false){ ?>
    <div>
        <br>
        <h1>Dodajte recepte tako da se ulogirate ili registrirate! :)</h1>
        <button type="button" id="btn"><a href="glavna.php">Nazad</a></button>
    </div>
    <?php } ?>
    </div>  
    
   
    <style>
        .heade{
            float: left;
            font-family: Ewert;
            font-size: 60px;
            color: black; 
            width: 100%;
            padding: 4px;
        }
        .kontenjer{
            text-align:center;
        }
        h1{
            font-family: Times New Roman;
            text-shadow: 2px 2px hotpink;
        
        }
        h2 {
            font-size: 50px;
        }
        a{
            text-decoration: none;
            color:white;
        }
        input{
            width: 25%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }
        textarea{
            width: 60%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            resize: vertical;
        }
        #btn{
            border: none;
            position: absolute;
            right: 50%;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
            background-color: rgb(245, 109, 245);
            color: ivory;
            border: 2px solid fuchsia;
            justify-content: center;
            align-items: center; 
        }
        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url("slike/junkyy.png");
            z-index: 2;
        }
        #fileToUpload{
            border: none;
            color: white;
            padding: 2px 2px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
            background-color: rgb(245, 109, 245);
            color: ivory;
            border: 2px solid fuchsia;
            justify-content: center;
            align-items: center; 
        }
        #predaj{
            border: none;
            color: white;
            padding: 2px 2px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            transition-duration: 0.4s;
            cursor: pointer;
            background-color: rgb(253, 116, 253);
            color: ivory;
            border: 2px solid fuchsia;
            justify-content: center;
            align-items: center; 
        }
        #fileToUpload:hover{
            background-color: rgb(243, 148, 222);
        }
        #predaj:hover{
            background-color: rgb(247, 164, 247);
        }
        #btn:hover{
            background-color: rgb(243, 148, 222);
        }
        a:hover{
            color:white;
            text-decoration:none;
        }


    </style>
     
    
</body>
</html>