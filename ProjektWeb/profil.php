<?php
include("connect.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profil</title>
    <!--font-->
    <link href='https://fonts.googleapis.com/css?family=Ewert' rel='stylesheet'>

    <!--bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
</head>
<body>
    <div class="overlay">
        <header class="heade">
            <div>
                <h2>Moj profil</h2>
            </div>
        </header>
        <?php if(isset($_SESSION["loggedin"])===true){   ?>
        <div>
            <div class="userinfo">
            <h3 id="naslovZaInfo">Moje informacije</h3>
                <?php
                    $id=$_SESSION["id"];      
                   // $query = "SELECT s.kime,s.email,s.datumrodjenja,s.faks,s.smjer,s.godstudiranja,r.ime,r.img FROM studenti AS s, recepti AS r";// WHERE s.id = '".$id."' ";
                    $query = "SELECT id,kime,email,datumrodjenja,faks,smjer,godstudiranja FROM studenti WHERE id=$id";// WHERE s.id = '".$id."' ";
                    
                    $result= mysqli_query($con,$query);
                    if($result){
                        while($row = mysqli_fetch_array($result)){
                            echo"<p id='ime'>".'Username: '.$row['kime']."</p>";
                            echo"<p id='mail'>".'Email: '.$row['email']."</p>";
                            echo"<p id='datum'>".'Datum rođenja: '.$row['datumrodjenja']."</p>";
                            echo"<p>Informacije o fakultetu:</p>";
                            echo"<p id='faks'>".$row['faks']."</p>";
                            echo"<p id='smjer'>".$row['smjer']."</p>";
                            echo"<p id='god'>".$row['godstudiranja'].'. godina'."</p>";
                                
                        }}?>
            </div>

            <div class="recepti">
                <h3 id="naslovZaNiz">Moji recepti</h3>
                <?php
                    $query = "SELECT ime,idstudenta,idR FROM recepti WHERE idstudenta=$id";
                    $result= mysqli_query($con,$query);
                    if($result){
                        while($row = mysqli_fetch_array($result)){
                            ?><tr> <td><?php
                            echo "<label id='imeRec'>".$row['ime']."</label>";
                            ?> </td> <td>
                            <a href="delete-process.php?idRecepta=<?php echo $row['idR']; ?>">Obriši</a>
                            </td> </tr>
                            <br>
                            <?php
                        }                      
                    }
                ?>               
            </div>
            
        </div> 
        
        <?php } ?>   
        
        <div id="naslovnica">
            <button type="button" class="btn btn-primary btn-lg"><a id="spec" href="http://localhost/ProjektWeb/glavna.php">Naslovnica</a></button>
        </div> 

    </div>
    
    <style>
        #spec{
            text-decoration:none;
            color:white;
            border:2px;
        }
        a{
            text-decoration:none;
            color:white;
            padding: 3px;
            border:2px solid rgb(187, 22, 132);
        }
        
        a:hover{
            color:white;
        }
        .heade{
            float: left;
            font-family: Ewert;
            color: black; 
            width: 100%;
            padding: 4px;
            margin: 8px;
        }
        h2{
            font-size:70px;
        }

        .overlay {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url("slike/junkyy.png");
            z-index: 2;
        }

        .userinfo{
            float:left;
            width:40%;
            padding:15px;
            box-sizing:border-box;
            background-color:white;
            font-family: "Times New Roman", Times, serif;
            font-size:20px;
            text-align:center;
        }
        #naslovZaNiz{
            font-family: Ewert;
            font-size:40px;
            color: white;
        }
        #naslovZaInfo{
            font-family: Ewert;
            font-size:40px;
            color: black;
        }
        .recepti{
            float:right;
            width:60%;
            padding:15px;
            box-sizing:border-box;
            background-color:rgb(235, 80, 235);
            font-family: "Times New Roman", Times, serif;
            font-size:18px;
            text-align:center;
            color:white;
            
        }
        #naslovnica{
            position: absolute;
            bottom: 20px;
            right: 20px;
            padding-left: 20px;
            padding-right: 20px;
        }

    </style>
</body>
</html>