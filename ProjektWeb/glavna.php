<?php
include("connect.php");
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Naslovnica Student Food</title>
    <link rel="stylesheet" type="text/css" href="zacss/css.css">
    <!--font-->
    <link href='https://fonts.googleapis.com/css?family=Ewert' rel='stylesheet'>
    <!--bootstrap-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!--upbtn and stars-->
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- rating -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href='https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/themes/fontawesome-stars.min.css' rel='stylesheet' type='text/css'>
    <!-- Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bar-rating/1.2.2/jquery.barrating.min.js" integrity="sha512-nUuQ/Dau+I/iyRH0p9sp2CpKY9zrtMQvDUG7iiVY8IBMj8ZL45MnONMbgfpFAdIDb7zS5qEJ7S056oE7f+mCXw==" crossorigin="anonymous"></script>

    
</head>
<body>

    <header>
        <div class="categoryPageToutDefault__image" style="background-image: url('zacss/cute2.jpg');">
            <h1 class="naslov">Stoo Food</h1> 
            <div class="side">
                <?php if(isset($_SESSION["loggedin"])===false){ ?>
                <div>
                <button type="button" id="btnPrijava"><a  href="prijava.html">Prijava</a></button>
                </div><div>
                <button type="button" id="btnReg"><a href="registracija.html">Registriraj se</a></button>
                <div>
                <?php } ?>
                <?php if(isset($_SESSION["loggedin"])===true){ ?>
                <div>
                <button type="button" id="btnOdjava"><a href="odjava.php">Odjava</a></button>
                </div>
                <?php } ?> 
            
                </div>  
            </div>
        </div>
    </header>
    <button onclick="topFunction()" id="upBtn"><i class='fas fa-angle-up' style='font-size:20px'></i></button> 
    
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://localhost/ProjektWeb/glavna.php">Naslovnica</a></li>
                    <li><a href="http://localhost/ProjektWeb/dodajrecept.php">Dodaj Recept</a></li>
                    <?php if(isset($_SESSION["loggedin"])===true){ ?>
                    <li><a href="http://localhost/ProjektWeb/profil.php">Profil</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
        
    <div>  
        <div>
            <header class="firstHeader" justify-content="center">
                <h2 class="title h3-text">Odaberi svoju stoodentsku hranu</h2>
            </header>
        </div>
    </div>
    <div class="container">
        <div class="recep">
            <?php
            $quer="SELECT r.idR,r.ime,r.img,r.opis,r.sastojci,r.idstudenta,s.kime,s.faks,s.id FROM recepti AS r INNER JOIN studenti AS s ON (r.idstudenta=s.id)";
            $response= mysqli_query($con,$quer);
                
            if($response){
                while($row=mysqli_fetch_array($response)){
                    $post_id = $row['idR'];
                    
                    
                    
                    echo "<div id='img_div'>";
                    echo "<img id='slike' src='uploads/".$row['img']."'>";
                    echo "<p id='naslovRec'>".$row['ime']."</p>";
                    ?>
                    <h3>Sastojci:<h3>
                    <?php
                    echo "<p id='sast'>".$row['sastojci']."</p>";
                    ?>
                    <h3>Priprema:<h3>
                    <?php
                    echo "<p id='opisRec'>".$row['opis']."</p>";

                    echo "<p id='userRecept'>".'by: '.$row['kime'].", ".$row['faks']."</p>";
                    echo"<br>";
                    echo"</div>";
                    ?>

                    <?php if(isset($_SESSION["loggedin"])===true){ ?>
                    <?php
                    $user_id=$_SESSION["id"];  
                    $query2 = "SELECT * FROM post_rating WHERE postid=$post_id AND userid=$user_id";
                    $userresult = mysqli_query($con,$query2) or die(mysqli_error());
                    $fetchRating = mysqli_fetch_array($userresult);
                    $rating = $fetchRating['rating'];
               
                    $query3 = "SELECT ROUND(AVG(rating),1) as averageRating FROM post_rating WHERE postid=$post_id";
                    $avgresult = mysqli_query($con,$query3) or die(mysqli_error());
                    $fetchAverage = mysqli_fetch_array($avgresult);
                    $averageRating = $fetchAverage['averageRating'];
                    if($averageRating <= 0){
                        $averageRating = "No rating yet.";
                    } 
                    ?>
                    <div class="post">
                        <div class="post-action">
                            <!-- Rating -->
                            <select class='rating' id='rating_<?php echo $post_id; ?>' data-id='rating_<?php echo $post_id; ?>'>
                                <option value="1" >1</option>
                                <option value="2" >2</option>
                                <option value="3" >3</option>
                                <option value="4" >4</option>
                                <option value="5" >5</option>
                            </select>
                            <?php if(isset($_SESSION["loggedin"])===true){ ?>
                            <div style='clear: both;'></div>
                            <label id="avgRat">Average Rating :</label> <span id='avgrating_<?php echo $post_id; ?>'><?php echo $averageRating; ?><label class="fas fa-star" id="jednaZvj"></label></span>
                            <!-- Set rating -->
                            <script type='text/javascript'>
                                $(document).ready(function(){
                                    $('#rating_<?php echo $post_id; ?>').barrating('set',<?php echo $rating; ?>);
                                });
                            </script>
                        </div>
                    </div>
                    <?php  }  } ?>

                    <br>
                    <?php                       
                    }
                }
            ?>    
            
            </div>
    </div>


       
        
    <script>
        $(function() {
                $('.rating').barrating({
                    theme: 'fontawesome-stars', 
                    onSelect: function(value, text, event) {
                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');
                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {
                    var split_id = el_id.split("_");
                    var post_id = split_id[1]; // post_id
                    
                    // AJAX Request
                    $.ajax({
                        url: 'store-star-rating.php',
                        type: 'post',
                        data: {post_id:post_id,rating:value},
                        dataType: 'json',
                        success: function(data){
                        // Update average
                        var average = data['averageRating'];
                        $('#avgrating_'+post_id).text(average);
                        }
                    });
                }
            }
            });
        });
  
        //Get the button
        var mybutton = document.getElementById("upBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            mybutton.style.display = "block";
        } else {
            mybutton.style.display = "none";
        }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
        window.pageYOffset=0;
        }
        
        
    </script>

    <style>       
        .container{
            justify-content:center;
            
        }
        #slike{
            border: 1px;
            border-radius: 4px;
            padding: 5px;
            width: 80%;
            filter: drop-shadow(8px 8px 10px black);
        }
        
        #img_div{
            width: 100%;
            text-align: center;
            padding: 5px;
            font-family: 'Times New Roman', Times, serif; 
            text-shadow: 2px 2px 5px hotpink;
        }
        #naslovRec{
            font-size: 30px;
            text-decoration:underline;

        }
        #opisRec, #sast{
            font-size:15px;
        }
        #userRecept{
            font-size:20px;
            font-style:italic;
            display:block;
            float:right;
            
        }
    
        .content{
            border: 0px solid black;
            border-radius: 3px;
            padding: 5px;
            margin: 0 auto;
            width: 50%;
        }
        .post{
            padding: 10px;
            margin-top: 10px;
            margin-bottom: 10px;
            text-align:center;
        }
        .post:last-child{
            border: 0;
        }
        .post h1{
            font-weight: normal;
            font-size: 30px;
        }
        .post a.link{
            text-decoration: none;
            color: black;
        }
        .post-text{
            letter-spacing: 1px;
            font-size: 15px;
            font-family: serif;
            color: gray;
            text-align: justify;
        }
        .post-action{
            margin-top: 15px;
            margin-bottom: 15px;
        }
        .like,.unlike{
            border: 0;
            background: none;
            letter-spacing: 1px;
            color: lightseagreen;
        }
        .like,.unlike:hover{
            cursor: pointer;
        }
        #avgRat{
            font-size:15px;
            color:purple;
        }
        #jednaZvj{
            color:orange;
            text-shadow: 2px 2px 3px grey;
        }
    </style>
 
</body>
</html>