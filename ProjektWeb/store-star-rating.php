<?php
    include("connect.php");
    session_start();
    $user_id = $_SESSION["id"]; 
    $post_id = $_POST['post_id'];
    $rating = $_POST['rating'];
    // Check entry within table
    $query = "SELECT COUNT(*) AS postCount FROM post_rating WHERE postid=".$post_id." userid=".$user_id;
    $result = mysqli_query($con,$query);
    $fetchdata = mysqli_fetch_array($result);
    $count = $fetchdata['postCount'];
    if($count == 0){
        $insertquery = "INSERT INTO post_rating(userid,postid,rating) values(".$user_id.",".$post_id.",".$rating.")";
        mysqli_query($con,$insertquery);
    }else {
        $updatequery = "UPDATE post_rating SET rating=" . $rating . " where userid=" . $user_id . " and postid=" . $post_id;
        mysqli_query($con,$updatequery);
    }
    // get average
    $query = "SELECT ROUND(AVG(rating),1) as averageRating FROM post_rating WHERE postid=".$post_id;
    $result = mysqli_query($con,$query) or die(mysqli_error($con));
    $fetchAverage = mysqli_fetch_array($result);
    $averageRating = $fetchAverage['averageRating'];
    $return_arr = array("averageRating"=>$averageRating);
    echo json_encode($return_arr);
?>