<?php
require_once "connect.php";
session_start();
$target_dir = "uploads/";

$uploadOk = 1;


if(isset($_POST["submit"])) {
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $naziv=filter_input(INPUT_POST,'naziv');
    $opis=filter_input(INPUT_POST,'opis');
    $sastojci=filter_input(INPUT_POST,'sastojci');
    $image = $_FILES['fileToUpload']['name'];
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    
    $userid=$_SESSION["id"];

    if($check !== false) {
      $uploadOk = 1;
    } else {
      echo "File is not an image.";
      $uploadOk = 0;
    }
  
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
    }
    if ($_FILES["fileToUpload"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
    }
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
    }

    if ($uploadOk == 0) {
      echo "<script>alert('Sorry, your file was not uploaded.')</script>";
      
    }else{
      if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                 
        $query="INSERT INTO recepti(ime,opis,img,sastojci,idstudenta) VALUES ('$naziv','$opis','$image','$sastojci','$userid')";
          
        $run_insert=mysqli_query($con,$query);
        if($run_insert){
            echo "<script>alert('Unijeli ste recept!')</script>";
        }else{
            echo "Error occured";
        }
        mysqli_close($con);
        echo "<script>window.open('glavna.php', '_self')</script>";
     
 }
}
  echo "<script>window.open('glavna.php', '_self')</script>";
}
?>